"use strict";// using strict
//creating objects of questions and binding in arrays
var questions = 
      [
        {
          question: "The nations involved in the war formed two opposing alliances. What were they called?",
          option: ['Axis and Allies', 'Homeland and Confederacy', 'Nazis and Freemen','None of above'],
          answer: 0,
          Explanation:"The two major powers were the Axis (Germany, Italy and Japan) and the Allies (the U.S., Britain, France, China and the USSR)."
        },
        
        {
          question: " About how many battle-wounded people were there worldwide during the course of World War II?",
          option: ['10 million', '25 million', '30 million','40 million'],
          answer: 1,
          Explanation:"Worldwide battle casualties are estimated at 25 million."
        },
        {
          question: " During the conflict, were there more  battle-related deaths",
          option: ['True', 'False'],
          answer: 1,
          Explanation:"The numbers are staggering, with an estimated 45 million civilian and 15 million battle-related deaths."
        },
        {
          question: " Robert Watson-Watt was working on a means of destroying enemy aircraft. His efforts resulted in the development of radar technology.",
          option: ['True', 'False'],
          answer: 0,
          Explanation:" In 1935, Robert Watson-Watt was working on a means of destroying enemy aircraft BUT  resulted in the development of radar technology."
        },
        {
          question: "_______________ was invented during the war to compensate for chocolate shortages",
          option: [''],
          answer: ['NUTELLA'],
          Explanation:"It is 'Nutella', a chocolate and hazelnut spread that is experiencing a resurgence of popularity."
        },
        {
          question: " Before Japan's surrender, there were plans for possibly launching a third atomic bomb.__________city was targeted for this final assault.",
          option: [''],
          answer: ['TOKYO'],
          Explanation: "After Nagasaki and Hiroshima, the third bomb was scheduled for release over Tokyo."
        },
      ];	

// display items on window onload
$(document).ready(function(){
  $(document).find(".firstQuiz").hide();

  // function by clicking on start button, diplays the questions
  $("#start").click(function(){
    $("h1, #start").hide();
    $(".firstQuiz").fadeOut().fadeIn(1000);
    displayQuestions();
    $(this).find(".error").hide();
   // $("#options").removeClass("disabled");
   });// end start button click
    
    //by clicking next button able to display the next question
  $("#next").click( function(){
    getAnswer();
   });// end next button click

  // function to check answers
  $("#checkAnswer").click(function(){
    checkAnswer();
      //$("#radio, #fillup").prop("disabled",'true');
    $("input[type=radio]").attr('disabled','true');// set attribute to disable by clicking
    $("input[type=textbox]").attr('disabled','true');
    });//end check answer click button

 // function to move on previous questions
  $("#previous").click(function(){
      goback();// function having functionality to move back to questions
    
  });// end previous button click
  
  // to quit quiz anytime during quiz
  $("#quit").click(function(){
    quizOver=true;
    $(".firstQuiz > .error").text("You attempted only: " +( parseInt(currentQuestion)+1) +" questions" );
    $(".error").show();
    displayScore();
  });// end quit button click
// to review questions
  $("#review").click(function(){
  
  $(".question").addClass("view");// adding class in review button

  });
});// end ready


// declaring the variables 
var currentQuestion=0;// to set the index of questions array to zero
var correctAnswer=0; // set the correct answers to zero at beginning
var quizOver=false;
var selections = [];// taking an array of answer chosen


// making all the functions

// function to display the questions as well as choices
function displayQuestions(){
  var question= questions[currentQuestion].question;
  var questionClass= $(document).find(".firstQuiz >.question")//identifying where to diplay question in html
  var optionList=$(document).find(".firstQuiz >#options")// specifying the section of choices to display
  var numOfoption=questions[currentQuestion].option.length;

  // set current question in the question class
 // $("#options").prop('disabled',false);
  $(questionClass).text(question).hide().slideDown(1000);
  $(optionList).find("li").remove();// Remove all current <li> elements of previous questions
  $(optionList).find("input").remove();// remove textbox of previous question
  $(".firstQuiz > .result").hide();
  $(".firstQuiz > .error").hide();
  $(".explanation").hide();// hiding the explanation section while loading next question
  $(".question").removeClass("view");// removing class of review on every new question
  
  var option;// setting up the display of options
  if (currentQuestion<=3){
    for(var i=0; i<numOfoption;i++){// loop to get options with radio buttons
      option= questions[currentQuestion].option[i];
      //console.log(options);
      if(selections[currentQuestion] == i) {
        $('<li><input type="radio" class="radio-inline" id="radio" checked="checked"  value=' + i + ' name="myoption" />' +  ' ' + option+ '</li>').appendTo(optionList);
      } 
      else {
        $('<li><input type="radio" class="radio-inline" id="radio" value=' + i + ' name="myoption" />' +  ' ' + option+ '</li>').appendTo(optionList);
      }
    }
  }
  else{// taking input from textbox
    $('<input type="textbox" id="fillup" value="" >').appendTo(optionList).focus();// showing textbox to fill up
    }
}// closing display question function

function getAnswer(){ // function to get answer and moving forwad with next questions
  if(!quizOver){
    var value;
    if (currentQuestion<=3){
      value = $("input[type='radio']:checked").val(); // getting the answer from radio buttons
      }
    else{
      value=$( "#fillup" ).val().toUpperCase();// value of fill up
      }
     
    if(value==undefined||value==""){// if user do not attempt the question
      if($(".question").hasClass("view")==true){
        currentQuestion++;
        condition();
        }
      else if($("input[type=radio]").prop('disabled')==false || $("input[type=textbox]").prop('disabled')==false ){// if check answer button is not clicked
        $(".error").text("Question is not attempted yet");// if the options are not disable then show error message
        $(".error").css({'background-color': '#F8C471'});
        $(".error").show().slideDown(3000);
        }
      else{
        console.log("buttons are disabled");// if button are disable then by clicking next can move to next question
        currentQuestion++;
        condition();
       }
    }
    else{
      $(".error").hide();
      if(value==questions[currentQuestion].answer){// matching values with answer already provided in questions object
        correctAnswer++;
        }
      selections[currentQuestion] = value;// taking value of answer in array
      currentQuestion++;
      if(currentQuestion >= 1) {
            $('#previous').prop("disabled", false);
          }
      if(currentQuestion<questions.length){
        displayQuestions();
          }
      else{
       
        displayScore();
         $(document).find("#next").val("One more try??");
         quizOver = true;
         
        }
      }
    }
  else{ // quiz is over and clicked the next button (which now displays 'Play Again?'
    quizOver = false;
    $(document).find("#next").val("Next");
    resetQuiz();
    displayQuestions();
    hideScore();
    }
    return value;
  }
  

//function to dislay the score
function displayScore() {
  $(".question, #options, .explanation,#previous,#review,#quit,#checkAnswer").hide();
  $(".firstQuiz > .result").text("You scored: " + correctAnswer + " out of: " + questions.length);
  $(".firstQuiz > .result").show();
 }

// function to make quiz reset 
function resetQuiz() {
  currentQuestion = 0;
  correctAnswer = 0;
  hideScore();
  selections=[];
  $(".question, #options, .explanation,#previous,#review,#quit,#checkAnswer").show();
}

function hideScore() {
    $(document).find(".result").hide();
}

function goback() {// previous button function
  if (!quizOver){
    if(currentQuestion == 0) {
      return false; // disable the action of previous button on first question
      }
      choose();// getting array of selected options.
      currentQuestion--; // moving to previous question
      if (currentQuestion < questions.length){
          displayQuestions();
      }           
    } 
  }

function choose() {// making array of selected answers
  selections[currentQuestion] = +$('input[name="answer"]:checked').val();
  }

function condition(){
  if(currentQuestion<questions.length){
    displayQuestions();
    }
  else{
    displayScore();
    $(document).find("#next").val("Play Again?");

    quizOver = true;
    }
}

function checkAnswer(){
   var explain= questions[currentQuestion].Explanation;
   var value;
    if (currentQuestion<=3){
      value = $("input[type='radio']:checked").val(); // getting the answer from radio buttons
      }
    else{
      value=$( "#fillup" ).val().toUpperCase();// value of fill up
      }
      if (value!= questions[currentQuestion].answer){
        if(value==undefined||value==""){
         $(".explanation").text(explain).slideToggle(1000);

      }
      else{
        $(".error").text("WRONG ANSWER :(");
        $(".error").css({'background-color': 'red'});
        $(".error").slideToggle(1000);
        $(".explanation").text(explain).slideToggle(1000);
      }
      }
      else if(value== questions[currentQuestion].answer){
        $(".error").text("CORRECT ANSWER :)");
        $(".error").css({'background-color': 'green'});
        $(".error").slideToggle(1000);
        $(".explanation").text(explain).slideToggle(1000);
      }
     

}


